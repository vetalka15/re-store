import * as actionTypes from '../types';

const booksRequested = () => {
  return {
    type: actionTypes.FETCH_BOOKS_REQUEST
  }
};

const booksLoaded = (newBooks) => {
  return {
    type: actionTypes.FETCH_BOOKS_SUCCESS,
    payload: newBooks
  }
};

const booksError = (error) => {
  return {
    type: actionTypes.FETCH_BOOKS_FAILURE,
    payload: error
  }
};

// const fetchBooksOld = (bookstoreService, dispatch) => () => {
//   dispatch(booksRequested());
//   bookstoreService.getBooks()
//     .then((data) => dispatch(booksLoaded(data)))
//     .catch((err) => dispatch(booksError(err)));
// };

const fetchBooks = (bookstoreService) => () => (dispatch) => {
  dispatch(booksRequested());
  bookstoreService.getBooks()
    .then((data) => dispatch(booksLoaded(data)))
    .catch((err) => dispatch(booksError(err)));
};

// book-table item
const bookAddedToCart = (bookId) => {
  return {
    type: actionTypes.BOOK_ADDED_TO_CART,
    payload: bookId
  }
};

const bookRemovedFromCart = (bookId) => {
  return {
    type: actionTypes.BOOK_REMOVED_FROM_CART,
    payload: bookId
  }
};

const allBooksRemovedFromCart = (bookId) => {
  return {
    type: actionTypes.ALL_BOOKS_REMOVED_FROM_CART,
    payload: bookId
  }
};

export {
  fetchBooks,
  bookAddedToCart,
  bookRemovedFromCart,
  allBooksRemovedFromCart
}